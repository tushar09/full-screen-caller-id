package com.android.internal.telephony;

/**
 * Created by Tushar on 7/12/2016.
 */
public interface ITelephony {
    boolean endCall();

    void answerRingingCall();

    void silenceRinger();
}
